from django.shortcuts import render, redirect
import pandas as pd
import plotly.express as px
import time
import datetime
from datetime import datetime
from .forms import PostForm

# checking the form and execution of the script
def create(request):
    if request.method == 'POST':
        form = PostForm(request.POST)

        if form.is_valid():
            post = form.save(commit=False)
            post.pub_date = datetime.now()
            post.save()

        ticker = 'UAH=X'
        period1 = request.POST.get("period1")
        utc_time = (time.mktime(datetime.strptime(period1, '%d-%m-%Y').timetuple()))
        inf = int(utc_time)
        period2 = request.POST.get("period2")
        utc_time_2 = (time.mktime(datetime.strptime(period2, '%d-%m-%Y').timetuple()))
        ind = int(utc_time_2)
        interval = '1d'
        url = f'https://query1.finance.yahoo.com/v7/finance/download/{ticker}?period1={inf}&period2={ind}&interval={interval}&events=history&includeAdjustedClose=true'
        df = pd.read_csv(url)
        fig = px.line(df, x='Date', y='Close', title='Apple Share Prices over time (2014)')
        fig.show()
        return render(request, 'create.html', {})


    else:
        form = PostForm()
        return render(request, 'create.html', {'form': form})
